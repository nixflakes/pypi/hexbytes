{
  description = "atsr python libraries";

  inputs = {
    settings = {
      url = "path:./nix/settings.nix";
      flake = false;
    };
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, flake-utils, ... }@inputs:

    flake-utils.lib.eachDefaultSystem (system: let

      settings = import inputs.settings;
      nixpkgs = inputs.nixpkgs.legacyPackages.${system};
      pythonPackages = nixpkgs.${settings.python+"Packages"};

      thispkg = pythonPackages.buildPythonPackage rec {
        pname = "hexbytes";
        version = "0.2.2";
        src = pythonPackages.fetchPypi {
          inherit pname version;
          sha256 = "a5881304d186e87578fb263a85317c808cf130e1d4b3d37d30142ab0f7898d03";
        };
      };

      pythonPackaged = nixpkgs.${settings.python}.withPackages (p: with p; [ thispkg ]);

    in {

      defaultPackage = thispkg;
        
      devShell = nixpkgs.mkShell {
        buildInputs = [ pythonPackaged ];
        shellHook = ''
          export PYTHONPATH=${pythonPackaged}/${pythonPackaged.sitePackages}
        '';
      };

    });
}
